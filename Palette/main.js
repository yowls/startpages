const time = document.querySelector("#time");
const date = document.querySelector("#date");

function updateClock(){
    let currentTime = new Date();
    let hours = currentTime.getHours();
    let minutes = currentTime.getMinutes();

    if(hours < 10){
        hours = "0" + hours;
    }

    if(minutes < 10){
        minutes = "0" + minutes;
    }

    time.textContent = hours + ":" + minutes
}

function updateDate(){
    let today = new Date();
    let day = today.getDay();
    let numberDay = today.getDate();
    let numberMonth = today.getMonth();
    let year = today.getFullYear();

    let week = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    let month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

    date.textContent = week[day-1] + " "+ numberDay + " / " + month[numberMonth] + " " + year
}

// Print clock and update every 5s
updateClock()
setInterval(() => {
    updateClock()
}, 5000);

// Print date
updateDate()


// Print Hex colors
let bodyStyles = window.getComputedStyle(document.body);
let c1 = bodyStyles.getPropertyValue('--color1')
let c2 = bodyStyles.getPropertyValue('--color2')
let c3 = bodyStyles.getPropertyValue('--color3')
let c4 = bodyStyles.getPropertyValue('--color4')
let c5 = bodyStyles.getPropertyValue('--color5')
let c6 = bodyStyles.getPropertyValue('--color6')

let boxC1 = document.getElementById('color1')
let boxC2 = document.getElementById('color2')
let boxC3 = document.getElementById('color3')
let boxC4 = document.getElementById('color4')
let boxC5 = document.getElementById('color5')
let boxC6 = document.getElementById('color6')

boxC1.textContent = c1
boxC2.textContent = c2
boxC3.textContent = c3
boxC4.textContent = c4
boxC5.textContent = c5
boxC6.textContent = c6